<?php

namespace Drupal\content_roles;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for content_role.
 */
class ContentRoleTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
