<?php

namespace Drupal\content_roles\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class RoleIsUniqueConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\user\Entity\User $user */
    $user = $items->getEntity();
    $content_roles = $user->content_roles->referencedEntities();
    $crids = [];

    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $content_role */
    foreach ($content_roles as $content_role) {
      if (in_array($content_role->id(), $crids)) {
        $this->context->addViolation($constraint->message, ['%user' => $user->getDisplayName()]);
        break;
      }
      else {
        $crids[] = $content_role->id();
      }
    }
  }

}
