<?php

namespace Drupal\content_roles\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Plugin implementation of the 'role_is_unique'.
 *
 * @Constraint(
 *   id = "RoleIsUnique",
 *   label = @Translation("The content role is unique"),
 * )
 */
class RoleIsUniqueConstraint extends Constraint {

  public $message = '%user already has this role.';

}
