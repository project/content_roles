<?php

namespace Drupal\content_roles\Plugin\EntityReferenceSelection;

use Drupal\content_roles\Service\ContentRolePermissionManager;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @EntityReferenceSelection(
 *   id = "content_roles_autocomplete",
 *   label = @Translation("Content roles autocomplete"),
 *   entity_types = {"content_role"},
 *   group = "content_roles_autocomplete",
 *   weight = 0
 * )
 */
class ContentRolesAutocomplete extends DefaultSelection {

  /**
   * The Content role permission manager service.
   *
   * @var \Drupal\content_roles\Service\ContentRolePermissionManager
   */
  protected $permissionManager;

  /**
   * ContentRolesAutocomplete constructor.
   */
  public function __construct(array $configuration, $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              ModuleHandlerInterface $module_handler,
                              AccountInterface $current_user,
                              ContentRolePermissionManager $content_role_permission_manager,
                              EntityFieldManagerInterface $entity_field_manager = NULL,
                              EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
                              EntityRepositoryInterface $entity_repository = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $module_handler, $current_user, $entity_field_manager, $entity_type_bundle_info, $entity_repository);

    $this->permissionManager = $content_role_permission_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration, $plugin_id,
                                $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('content_roles.permission_manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    if (!$this->currentUser->hasPermission('administer content roles')) {
      $user = $this->entityTypeManager->getStorage('user')->load
      ($this->currentUser->id());
      $available_content_role_types = $this->permissionManager->getUsersManagableContentRoleTypeIds($user);

      if (!empty($available_content_role_types)) {
        $query->condition('type', $available_content_role_types, 'IN');
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferenceableEntities(array $ids) {
    // @todo: This needs to more sophisticated.
    $result = [];
    if ($ids) {
      $query = $query = parent::buildEntityQuery();
      $result = $query
        ->condition('id', $ids, 'IN')
        ->execute();
    }
    return $result;
  }

}
