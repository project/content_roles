<?php

namespace Drupal\content_roles\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'content_role_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "content_role_field_widget",
 *   module = "content_roles",
 *   label = @Translation("Content role field widget"),
 *   field_types = {
 *     "map"
 *   }
 * )
 */
class ContentRoleFieldWidget extends WidgetBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\content_roles\Entity\ContentRole $entity */
    $entity = $items->getParent()->getValue();
    /** @var \Drupal\Core\Config\ImmutableConfig $type_config */
    $type_config = \Drupal::configFactory()->get('content_roles.content_role_type.' . $entity->bundle());
    $permission_keys = $type_config->get('permissions');
    $core_permissions = \Drupal::service('user.permissions')->getPermissions();

    $permissions = $entity->permissions->getValue();
    foreach ($core_permissions as $key => $permission) {
      if (in_array($key, $permission_keys)) {
        $element[$key] = [
          '#type' => 'checkbox',
          '#title' => $permission['title'],
          '#default_value' => !empty($permissions[0][$key]) ? $permissions[0][$key] : 0,
          '#description' => !empty($core_permissions[$key]['restrict access']) ? $this->t('Warning: Give to trusted roles only; this permission has security implications.') : '',
        ];
      }
    }

    return $element;
  }

}
