<?php

namespace Drupal\content_roles\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Content role entity.
 *
 * @ingroup content_roles
 *
 * @ContentEntityType(
 *   id = "content_role",
 *   label = @Translation("Content role"),
 *   bundle_label = @Translation("Content role type"),
 *   handlers = {
 *     "storage" = "Drupal\content_roles\ContentRoleStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\content_roles\ContentRoleListBuilder",
 *     "views_data" = "Drupal\content_roles\Entity\ContentRoleViewsData",
 *     "translation" = "Drupal\content_roles\ContentRoleTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\content_roles\Form\ContentRoleForm",
 *       "add" = "Drupal\content_roles\Form\ContentRoleForm",
 *       "edit" = "Drupal\content_roles\Form\ContentRoleForm",
 *       "delete" = "Drupal\content_roles\Form\ContentRoleDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\content_roles\ContentRoleHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\content_roles\ContentRoleAccessControlHandler",
 *   },
 *   base_table = "content_role",
 *   data_table = "content_role_field_data",
 *   revision_table = "content_role_revision",
 *   revision_data_table = "content_role_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer content roles",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/content_role/{content_role}",
 *     "add-page" = "/admin/content/content_role/add",
 *     "add-form" = "/admin/content/content_role/add/{content_role_type}",
 *     "edit-form" = "/admin/content/content_role/{content_role}/edit",
 *     "delete-form" = "/admin/content/content_role/{content_role}/delete",
 *     "version-history" = "/admin/content/content_role/{content_role}/revisions",
 *     "revision" = "/admin/content/content_role/{content_role}/revisions/{content_role_revision}/view",
 *     "revision_revert" = "/admin/content/content_role/{content_role}/revisions/{content_role_revision}/revert",
 *     "revision_delete" = "/admin/content/content_role/{content_role}/revisions/{content_role_revision}/delete",
 *     "translation_revert" = "/admin/content/content_role/{content_role}/revisions/{content_role_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/content_role",
 *   },
 *   bundle_entity_type = "content_role_type",
 *   field_ui_base_route = "entity.content_role_type.edit_form"
 * )
 */
class ContentRole extends EditorialContentEntityBase implements ContentRoleInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use StringTranslationTrait;

  /**
   * @var \Drupal\user\PermissionHandler
   */
  protected $permissionHandler;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $this->cleanupPermissions();

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the content_role owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }

    $this->setNewRevision(TRUE);
    $this->setRevisionTranslationAffected(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    $value = $this->cleanupPermissions();
    return !empty($value[0]) ? $value[0] : NULL;
  }

  /**
   * Remove every 0 value pemissions.
   */
  private function cleanupPermissions() {
    $enabled_permissions = [];
    /** @var \Drupal\user\PermissionHandler $permission_handler */
    $permission_handler = \Drupal::service('user.permissions');
    $all_permissions = $permission_handler->getPermissions();
    $values = $this->get('permissions')->getValue();
    foreach ($values[0] as $index => $value) {
      if ($value != 0 && array_key_exists($index, $all_permissions)) {
        $enabled_permissions[0][$index] = 1;
      }
    }
    $this->set('permissions', $enabled_permissions);
    return $enabled_permissions;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Content role entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Content role entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['permissions'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Permissions'))
      ->setDescription(t('The serialized permission values.'))
      ->setDisplayOptions('form', [
        'type' => 'content_role_field_widget',
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'type' => 'content_role_field_formatter',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setLabel(t('Active'))
      ->setDescription(t('A boolean indicating whether the Content role is active.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
