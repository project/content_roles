<?php

namespace Drupal\content_roles\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Content role type entity.
 *
 * @ConfigEntityType(
 *   id = "content_role_type",
 *   label = @Translation("Content role type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\content_roles\ContentRoleTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\content_roles\Form\ContentRoleTypeForm",
 *       "edit" = "Drupal\content_roles\Form\ContentRoleTypeForm",
 *       "delete" = "Drupal\content_roles\Form\ContentRoleTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\content_roles\ContentRoleTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "content_role_type",
 *   admin_permission = "administer content role types",
 *   bundle_of = "content_role",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/content_role_type/{content_role_type}",
 *     "add-form" = "/admin/structure/content_role_type/add",
 *     "edit-form" = "/admin/structure/content_role_type/{content_role_type}/edit",
 *     "delete-form" = "/admin/structure/content_role_type/{content_role_type}/delete",
 *     "collection" = "/admin/structure/content_role_type"
 *   },
 *   config_export = {
 *     "label",
 *     "type",
 *     "description",
 *     "permissions",
 *   }
 * )
 */
class ContentRoleType extends ConfigEntityBundleBase implements ContentRoleTypeInterface {

  /**
   * The Content role type ID.
   *
   * @var string
   */
  protected $type;

  /**
   * The Content role type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this Content role type.
   *
   * @var string
   */
  protected $description;

  /**
   * An array of the machine names of the allowed permissions.
   *
   * @var array
   */
  protected $permissions;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  public function getPermissions() {
    return !empty($this->permissions) ? $this->permissions : [];
  }

  public function setPermissions(array $permissions) {
    $this->permissions = $permissions;
  }

}
