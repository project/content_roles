<?php

namespace Drupal\content_roles\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Content role entities.
 *
 * @ingroup content_roles
 */
interface ContentRoleInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Content role name.
   *
   * @return string
   *   Name of the Content role.
   */
  public function getName();

  /**
   * Sets the Content role name.
   *
   * @param string $name
   *   The Content role name.
   *
   * @return \Drupal\content_roles\Entity\ContentRoleInterface
   *   The called Content role entity.
   */
  public function setName($name);

  /**
   * Gets the Content role creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Content role.
   */
  public function getCreatedTime();

  /**
   * Sets the Content role creation timestamp.
   *
   * @param int $timestamp
   *   The Content role creation timestamp.
   *
   * @return \Drupal\content_roles\Entity\ContentRoleInterface
   *   The called Content role entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Content role revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Content role revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\content_roles\Entity\ContentRoleInterface
   *   The called Content role entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Content role revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Content role revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\content_roles\Entity\ContentRoleInterface
   *   The called Content role entity.
   */
  public function setRevisionUserId($uid);

}
