<?php

namespace Drupal\content_roles\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Content role entities.
 */
class ContentRoleViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
