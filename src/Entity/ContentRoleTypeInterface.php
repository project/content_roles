<?php

namespace Drupal\content_roles\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Content role type entities.
 */
interface ContentRoleTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
