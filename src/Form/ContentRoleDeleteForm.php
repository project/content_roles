<?php

namespace Drupal\content_roles\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Content role entities.
 *
 * @ingroup content_roles
 */
class ContentRoleDeleteForm extends ContentEntityDeleteForm {


}
