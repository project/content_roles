<?php

namespace Drupal\content_roles\Form;

use Drupal\content_roles\Entity\ContentRole;
use Drupal\content_roles\Service\ContentRolesManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AddUserToContentRole.
 */
class AddUserToContentRole implements FormInterface, ContainerInjectionInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatch
   */
  protected $routeMatch;

  /**
   * The content roles manager service.
   *
   * @var \Drupal\content_roles\Service\ContentRolesManager
   */
  protected $contentRolesManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */

  protected $messenger;

  /**
   * The user entity storage.
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * The content role entity.
   *
   * @var ContentRole
   */
  protected $contentRole;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * AddUserToContentRole constructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManager $entityTypeManager,
                              CurrentRouteMatch $routeMatch,
                              ContentRolesManager $contentRolesManager,
                              Messenger $messenger,
                              AccountProxy $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $routeMatch;
    $this->contentRolesManager = $contentRolesManager;
    $this->messenger = $messenger;
    $this->userStorage = $this->entityTypeManager->getStorage('user');
    $content_role = $this->routeMatch->getParameter('content_role');
    if (is_string($content_role)) {
      $this->contentRole = $this->entityTypeManager->getStorage('content_role')
        ->load($content_role);
    }
    else {
      $this->contentRole = $content_role;
    }

    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('content_roles.manager'),
      $container->get('messenger'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_user_to_content_role';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ContentRole $contentRole = NULL) {
    $form['user'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('Select a user'),
      '#weight' => '0',
      '#default_value' => '',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add user to @role', ['@role' => $this->contentRole->label()]),
      '#weight' => '100',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\Entity\User $user */
    $user = $this->userStorage->load($form_state->getValue('user'));
    $content_roles = $user->content_roles->referencedEntities();
    foreach ($content_roles as $content_role) {
      if ($content_role == $this->contentRole) {
        $form_state->setErrorByName('user', $this->t('%user already has this role.', ['%user' => $user->getDisplayName()]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\Entity\User $account */
    $account = $this->userStorage->load($form_state->getValue('user'));
    if ($account) {
      try {
        $this->contentRolesManager->addUserToContentRole($account, $this->contentRole, TRUE);
        $this->messenger->addMessage($this->t('@user has been added to the @role', ['@user' => $account->getDisplayName(), '@role' => $this->contentRole->label()]));
      }
      catch (\Exception $e) {
        $this->messenger->addError($this->t('Oups, something went wrong. Contact the system administrator.'));
        // @todo: Add logger.
      }
    }
  }

  /**
   * Access callback.
   */
  public function access() {
    $account = $this->currentUser;
    if (!is_null($this->contentRole)) {
      $type_id = $this->contentRole->bundle();
      $manager_permission = "manage $type_id content role";

      return AccessResult::allowedIfHasPermissions(
        $account,
        [
          $this->contentRole->getEntityType()->getAdminPermission(),
          $manager_permission
        ]
        , 'OR'
      );
    }

    return AccessResult::neutral();

  }

  /**
   * Title callback.
   */
  public function title() {
    return $this->t('Add user to %role', ['%role' => $this->contentRole->label()]);
  }

}
