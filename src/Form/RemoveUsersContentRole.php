<?php

namespace Drupal\content_roles\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\content_roles\Entity\ContentRole;
use Drupal\content_roles\Service\ContentRolesManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete Content role type entities.
 */
class RemoveUsersContentRole  extends FormBase {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatch
   */
  protected $routeMatch;

  /**
   * The content roles manager service.
   *
   * @var \Drupal\content_roles\Service\ContentRolesManager
   */
  protected $contentRolesManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The content role entity storage.
   *
   * @var \Drupal\content_roles\ContentRoleStorage
   */
  protected $contentRoleStorage;

  /**
   * The user entity storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * The content role entity.
   *
   * @var ContentRole
   */
  protected $contentRole;

  /**
   * The user entity.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $account;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface     $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface          $time
   *   The time service.
   */
  public function __construct(EntityTypeManager $entityTypeManager,
                              CurrentRouteMatch $routeMatch,
                              ContentRolesManager $contentRolesManager,
                              Messenger $messenger,
                              AccountProxy $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $routeMatch;
    $this->contentRolesManager = $contentRolesManager;
    $this->messenger = $messenger;
    $this->contentRole = $routeMatch->getParameter('content_role');
    $this->account = $routeMatch->getParameter('user');
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('content_roles.manager'),
      $container->get('messenger'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_role_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %user from %role?', [
      '%user' => $this->account->getDisplayName(),
      '%role' => $this->contentRole->label()
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getCancelUrl() {
    // Prepare cancel link.
    $query = $this->getRequest()->query;
    $url = NULL;
    // If a destination is specified, that serves as the cancel link.
    if ($query->has('destination')) {
      $options = UrlHelper::parse($query->get('destination'));
      // @todo Revisit this in https://www.drupal.org/node/2418219.
      try {
        $url = Url::fromUserInput('/' . ltrim($options['path'], '/'), $options);
      }
      catch (\InvalidArgumentException $e) {
        // Suppress the exception and fall back to the form's cancel url.
      }
    }
    // Check for a route-based cancel link.
    if (!$url) {
      $url = $this->contentRole->toUrl('canonical');;
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Confirm');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormName() {
    return $this->t('Remove');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->getQuestion();
    $form['#attributes']['class'][] = 'confirmation';
    $form['description'] = ['#markup' => '<div>' . $this->getDescription() . '</div>'];
    $form['#theme'] = 'confirm_form';
    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    return [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->getConfirmText(),
        '#submit' => [
          [$this, 'submitForm'],
        ],
      ],
      'cancel' => $this->buildCancelLink(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Override the default validation implementation as it is not necessary
    // nor possible to validate an entity in a confirmation form.
    return $this->account;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addMessage(
      $this->t('Removed %user from %role.', [
        '%user' => $this->account->getDisplayName(),
        '%label' => $this->contentRole->label(),
      ])
    );

    $this->contentRolesManager->removeUserFromContentRole($this->account, $this->contentRole, TRUE);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * Access callback.
   */
  public function access() {
    $account = $this->currentUser;
    $type_id = $this->contentRole->bundle();
    $manager_permission = "manage $type_id content role";

    return AccessResult::allowedIfHasPermissions(
      $account, [
      $this->contentRole->getEntityType()->getAdminPermission(),
      $manager_permission
    ], 'OR'
    );
  }

  /**
   * The title callback.
   */
  public function title() {
    return $this->getQuestion();
  }

  /**
   * Builds the cancel link for a confirmation form.
   *
   * @return array
   *   The link render array for the cancel form.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function buildCancelLink() {
    return [
      '#type' => 'link',
      '#title' => $this->getCancelText(),
      '#attributes' => ['class' => ['button']],
      '#url' => $this->getCancelUrl(),
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];
  }

}
