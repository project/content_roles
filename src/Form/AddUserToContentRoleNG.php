<?php

namespace Drupal\content_roles\Form;

use Drupal\content_roles\Entity\ContentRole;
use Drupal\content_roles\Service\ContentRolesManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AddUserToContentRole.
 */
class AddUserToContentRole implements FormInterface, ContainerInjectionInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatch
   */
  protected $routeMatch;

  /**
   * The content roles manager service.
   *
   * @var \Drupal\content_roles\Service\ContentRolesManager
   */
  protected $contentRolesManager;

  /**
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;


  /**
   * AddUserToContentRole constructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManager $entityTypeManager,
                              CurrentRouteMatch $routeMatch,
                              ContentRolesManager $contentRolesManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $routeMatch;
    $this->contentRolesManager = $contentRolesManager;
    $this->userStorage = $this->entityTypeManager->getStorage('user');
    $this->contentRole = $this->routeMatch->getParameter('content_role');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('content_roles.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_user_to_content_role';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ContentRole $contentRole = NULL) {
    $under_confirm = $form_state->getTemporaryValue('under_confirm');
    if (!$under_confirm) {
      $form['user'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'user',
        '#title' => $this->t('Select a user'),
        '#weight' => '0',
        '#default_value' => '',
      ];
      $form['add_user'] = [
        '#type' => 'button',
        '#value' => $this->t('Add user'),
        '#weight' => '0',
        '#ajax' => [
          'callback' => '::updateSelectedUsers',
          'disable-refocus' => FALSE,
          'event' => 'click',
          'wrapper' => 'try',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Adding user..'),
          ],
        ],
      ];
    }
    $form['selected_users'] = [
      '#markup' => '<div id="selected-users"></div>',
    ];
    $form['selected_user_ids'] = [
      '#type' => 'hidden',
      '#weight' => '0',
      '#defaul_value' => $form_state->getValue('selected_user_ids'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#weight' => '100',
    ];
    $form['#prefix'] = '<div id="try">';
    $form['#suffix'] = '</div>';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#type'] == 'submit') {
      $form_state->setTemporaryValue('under_confirm', TRUE);
    }
    else {
      $uid = $form_state->getValue('user');
      $a = $form_state->getValue('selected_user_ids');
      $form_state->setValue('selected_user_ids', $form_state->getValue('selected_user_ids') . $uid . '|');
      $e = $form_state->getTemporaryValue('selected_user_ids');
      $form_state->setTemporaryValue('selected_user_ids', $form_state->getTemporaryValue('selected_user_ids') . $uid . '|');
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#type'] == 'submit') {
      if ($form_state->get('under_form_state')) {
        $accounts = $this->getSelectedUsers($form_state);
        foreach ($accounts as $account) {
          $this->contentRolesManager->addUserToContentRole($account, $this->contentRole, TRUE);
        }
      }
    }
  }

  /**
   * Access callback.
   */
  public function access() {
    return AccessResult::allowed();
  }

  /**
   * Title callback.
   */
  public function title() {
    return $this->t('Add users to @role', []);
  }

  public function updateSelectedUsers(array &$form, FormStateInterface &$form_state) {
    $accounts = $this->getSelectedUsers($form_state);

    $items = [];
    /** @var \Drupal\user\Entity\User $account */
    foreach ($accounts as $account) {
      $items[] = [
        '#type' => 'html_tag',
        '#tag' => 'li',
        '#value' => $account->getDisplayName(),
        '#prefix' => '<ul>',
        '#suffix' => '</ul>',
      ];
    }

    $form['selected_users'] = [
      '#type' => 'item',
      '#title' => $this->t('Selected users'),
      '#wrapper_attributes' => [
        'id' => 'selected-users',
      ],
      $items,
    ];

    return $form;
  }

  private function getSelectedUsers(FormStateInterface $form_state) {
    if ($form_state->getTemporaryValue('selected_user_ids')) {
      $uids = explode('|', $form_state->getValue('selected_user_ids'));
      return $this->userStorage->loadMultiple($uids);
    }
    return FALSE;
  }

}
