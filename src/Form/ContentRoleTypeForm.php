<?php

namespace Drupal\content_roles\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ContentRoleTypeForm.
 */
class ContentRoleTypeForm extends BundleEntityFormBase {

  use StringTranslationTrait;

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * Constructs a new UserPermissionsForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface       $permission_handler
   *   The permission handler.
   * @param \Drupal\user\RoleStorageInterface             $role_storage
   *   The role storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(PermissionHandlerInterface $permission_handler) {
    $this->permissionHandler = $permission_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\content_roles\Entity\ContentRoleType $type */
    $type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $type->label(),
      '#description' => $this->t("Label for the Content role type."),
      '#required' => TRUE,
    ];

    $form['type'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\content_roles\Entity\ContentRoleType::load',
      ],
      '#disabled' => !$type->isNew(),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $type->getDescription(),
    ];

    $permissions = $this->permissionHandler->getPermissions();

    $permissions_by_provider = [];
    foreach ($permissions as $permission_name => $permission) {
      $permissions_by_provider[$permission['provider']][$permission_name] = $permission;
    }

    $form['permissions'] = [
      '#type' => 'details',
      '#title' => $this->t('Allowed permissions'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    foreach ($permissions_by_provider as $provider => $permissions) {
      $form['permissions'][$provider] = [
        '#markup' => '<h3>' . $this->moduleHandler->getName($provider) . '</h3>',
      ];

      foreach ($permissions as $perm => $perm_item) {
        $form['permissions'][$perm] = [
          '#title' => $perm_item['title'],
          '#description' => !empty($perm_item['restrict access']) ? $this->t('Warning: Give to trusted roles only; this permission has security implications.') : '',
          '#wrapper_attributes' => [
            'class' => ['checkbox'],
          ],
          '#type' => 'checkbox',
          '#default_value' => in_array($perm, $type->getPermissions()) ? 1 : 0,
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $type = $this->entity;
    $permissions = [];
    // Remove the unnecessary permissions.
    foreach ($form_state->getValue('permissions') as $perm => $value) {
      if ($value) {
        $permissions[] = $perm;
      }
    }

    $type->setPermissions($permissions);
    $status = $type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Content role type.', [
          '%label' => $type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Content role type.', [
          '%label' => $type->label(),
        ]));
    }
    $form_state->setRedirectUrl($type->toUrl('collection'));
  }

}
