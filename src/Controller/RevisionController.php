<?php

namespace Drupal\content_roles\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\content_roles\Entity\ContentRoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RevisionController.
 *
 *  Returns responses for Content role routes.
 */
class RevisionController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Content role revision.
   *
   * @param int $content_role_revision
   *   The Content role revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($content_role_revision) {
    $content_role = $this->entityTypeManager()->getStorage('content_role')
      ->loadRevision($content_role_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('content_role');

    return $view_builder->view($content_role);
  }

  /**
   * Page title callback for a Content role revision.
   *
   * @param int $content_role_revision
   *   The Content role revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($content_role_revision) {
    $content_role = $this->entityTypeManager()->getStorage('content_role')
      ->loadRevision($content_role_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $content_role->label(),
      '%date' => $this->dateFormatter->format($content_role->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Content role.
   *
   * @param \Drupal\content_roles\Entity\ContentRoleInterface $content_role
   *   A Content role object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ContentRoleInterface $content_role) {
    $account = $this->currentUser();
    $content_role_storage = $this->entityTypeManager()->getStorage('content_role');

    $langcode = $content_role->language()->getId();
    $langname = $content_role->language()->getName();
    $languages = $content_role->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $content_role->label()]) : $this->t('Revisions for %title', ['%title' => $content_role->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all content role revisions") || $account->hasPermission('administer content roles')));
    $delete_permission = (($account->hasPermission("delete all content role revisions") || $account->hasPermission('administer content roles')));

    $rows = [];

    $vids = $content_role_storage->revisionIds($content_role);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\content_roles\ContentRoleInterface $revision */
      $revision = $content_role_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $content_role->getRevisionId()) {
          $link = $this->l($date, new Url('entity.content_role.revision', [
            'content_role' => $content_role->id(),
            'content_role_revision' => $vid,
          ]));
        }
        else {
          $link = $content_role->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.content_role.translation_revert', [
                'content_role' => $content_role->id(),
                'content_role_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.content_role.revision_revert', [
                'content_role' => $content_role->id(),
                'content_role_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.content_role.revision_delete', [
                'content_role' => $content_role->id(),
                'content_role_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['content_role_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
