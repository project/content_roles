<?php
/**
 * @file
 * Contains Drupal\content_roles\Controller.
 */


namespace Drupal\content_roles\Controller;


use Drupal\content_roles\Service\ContentRolesManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ContentRolesControllerBase implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The Current user service.
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The content roles manager service.
   *
   * @var \Drupal\content_roles\Service\ContentRolesManager
   */
  protected $contentRolesManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatch
   */
  protected $routeMatch;

  /**
   * The content role storage.
   *
   * @var \Drupal\content_roles\ContentRoleStorage
   */
  protected $contentRoleStorage;

  /**
   * Constructs a UsersPermissionsController object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManager $entityTypeManager,
                              AccountProxyInterface $currentUser,
                              ContentRolesManager $contentRolesManager,
                              CurrentRouteMatch $routeMatch) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->contentRolesManager = $contentRolesManager;
    $this->routeMatch = $routeMatch;
    $this->contentRoleStorage= $this->entityTypeManager->getStorage('content_role');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('content_roles.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * The render callback.
   */
  abstract function view();

  /**
   * The controller's access method.
   */
  abstract function access();

  /**
   * The title callback.
   */
  abstract function title();

}
