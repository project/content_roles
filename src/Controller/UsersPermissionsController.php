<?php

namespace Drupal\content_roles\Controller;


use Drupal\content_roles\Service\ContentRolesManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Session\AccountProxy;

/**
 * Contains Drupal\content_roles\Controller.
 */
class UsersPermissionsController extends ContentRolesControllerBase {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface;
   */
  protected $userStorage;

  /**
   * UsersPermissionsController constructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManager $entityTypeManager,
                              AccountProxy $currentUser,
                              ContentRolesManager $contentRolesManager,
                              CurrentRouteMatch $routeMatch) {
    parent::__construct($entityTypeManager, $currentUser, $contentRolesManager, $routeMatch);
    $this->userStorage = $this->entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function view() {
    $build = [];
    $account = $this->routeMatch->getParameter('user');
    $all_permissions = $this->contentRolesManager->getAllPermissions();
    $grouped_permissions = $this->contentRolesManager->getUsersGroupedPermissions($account);

    if (!empty($grouped_permissions['config_roles'])) {
      $build['config_roles'] = [
        '#type' => 'details',
        '#title' => $this->t('Permissions by configuration roles'),
        '#open' => TRUE,
      ];
    }

    if (!empty($grouped_permissions['content_roles'])) {
      $build['content_roles'] = [
        '#type' => 'details',
        '#title' => $this->t('Permissions by content roles'),
        '#open' => TRUE,
      ];
    }
    foreach ($grouped_permissions as $key => $roles) {
      if ($key == 'config_roles') {
        foreach ($roles as $rid => $permissions) {
          $role = $this->contentRolesManager->loadConfigRole($rid);
          $items = [];
          foreach ($permissions as $permission_key) {
            if (!empty($all_permissions[$permission_key])) {
              $items[] = [
                '#type' => 'inline_template',
                '#template' => "<div class=\"permission\"><span class=\"title\">
{{label}}</span>
<div class=\"description\">{{description}}
<em class=\"permission-warning\">{{warning}}</em>
</div></div>",
                '#context' => [
                  'label' => $all_permissions[$permission_key]['title'],
                  'description' => $all_permissions[$permission_key]['description'],
                  'warning' => !empty($all_permissions[$permission_key]['restrict access']) ? $this->t('Warning: Give to trusted roles only; this permission has security implications.') : '',
                ],
              ];
            }
          }
          $build[$key][$rid] = [
            '#theme' => 'item_list',
            '#list_type' => 'ul',
            '#title' => $role->get('label'),
            '#items' => $items,
          ];
        }
      }
      else {
        foreach ($roles as $rid => $permissions) {
          $items = [];
          $role = $this->contentRoleStorage->load($rid);

          foreach ($permissions as $permission_key => $value) {
            if (!empty($all_permissions[$permission_key])) {
              $items[] = [
                '#type' => 'inline_template',
                '#template' => "<div class=\"permission\"><span class=\"title\">
{{label}}</span>
<div class=\"description\">{{description}}
<em class=\"permission-warning\">{{warning}}</em>
</div></div>",
                '#context' => [
                  'label' => $all_permissions[$permission_key]['title'],
                  'description' => $all_permissions[$permission_key]['description'],
                  'warning' => !empty($all_permissions[$permission_key]['restrict access']) ? $this->t('Warning: Give to trusted roles only; this permission has security implications.') : '',
                ],
              ];
            }
          }
          $build[$key][$rid] = [
            '#theme' => 'item_list',
            '#list_type' => 'ul',
            '#title' => $role->label(),
            '#items' => $items,
          ];
        }
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    return AccessResult::allowedIf($this->currentUser->hasPermission('access user permissions overview'));
  }

  /**
   * {@inheritdoc}
   */
  public function title() {
    return $this->t('Permissions of @user', ['@user' => $this->routeMatch->getParameter('user')->getDisplayName()]);
  }

}
