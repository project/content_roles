<?php

namespace Drupal\content_roles\Service;

use Drupal\content_roles\Entity\ContentRoleType;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;

use Drupal\user\Entity\User;
use Drupal\user\PermissionHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides dynamic permissions for Content role of different types.
 *
 * @ingroup content_roles
 *
 */
class ContentRolePermissionManager implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The user.permissions service.
   *
   * @var \Drupal\user\PermissionHandler
   */
  protected $permissionHandler;

  /**
   * The Content role type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|mixed|object
   */
  protected $contentRoleTypeStorage;

  /**
   * ContentRolePermissionManager constructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManager $entityTypeManager,
                              AccountProxy $currentUser,
                              PermissionHandler $permissionHandler) {
    $this->currentUser = $currentUser;
    $this->permissionHandler = $permissionHandler;
    $this->contentRoleTypeStorage = $entityTypeManager->getStorage('content_role_type');
  }

  /**
   * @inheritdoc
   *
   * However it is a service, we have to set up the dependency injection,
   * because it is used a permission callback.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('user.permissions')
    );
  }

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The ContentRole by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];

    foreach ($this->contentRoleTypeStorage->loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of content role permissions for a given content role type.
   *
   * @param \Drupal\content_roles\Entity\ContentRole $type
   *   The ContentRole type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(ContentRoleType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "$type_id create entities" => [
        'title' => $this->t('%type_name: Create new roles', $type_params),
      ],
      "$type_id edit own entities" => [
        'title' => $this->t('%type_name: Edit own roles', $type_params),
      ],
      "$type_id edit any entities" => [
        'title' => $this->t('%type_name: Edit any roles', $type_params),
      ],
      "$type_id delete own entities" => [
        'title' => $this->t('%type_name: Delete own roles', $type_params),
      ],
      "$type_id delete any entities" => [
        'title' => $this->t('%type_name: Delete any roles', $type_params),
      ],
      "$type_id view revisions" => [
        'title' => $this->t('%type_name: View revisions', $type_params),
        'description' => $this->t('To view a revision, you also need permission to view the entity item.'),
      ],
      "$type_id revert revisions" => [
        'title' => $this->t('%type_name: Revert revisions', $type_params),
        'description' => $this->t('To revert a revision, you also need permission to edit the entity item.'),
      ],
      "$type_id delete revisions" => [
        'title' => $this->t('%type_name: Delete revisions', $type_params),
        'description' => $this->t('To delete a revision, you also need permission to delete the entity item.'),
      ],
      "manage $type_id content role" => [
        'title' => $this->t('%type_name: Add/remove role to a user', $type_params),
      ],
    ];
  }

  /**
   * Gets manageble content role types.
   *
   * Gets an array of content role types ids in the user has permission to
   * add/remove users.
   */
  public function getUsersManagableContentRoleTypeIds(User $user) {
    $group_role_types = $this->contentRoleTypeStorage->loadMultiple();
    $return = [];

    foreach ($group_role_types as $type) {
      $type_id = $type->id();
      if ($user->hasPermission("manage $type_id content role")) {
        $return[] = $type_id;
      }
    }

    return $return;
  }

}
