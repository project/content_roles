<?php

namespace Drupal\content_roles\Service;

use Drupal\content_roles\Entity\ContentRole;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\PermissionHandlerInterface;
use Drupal\content_roles\Service\ContentRolesManager;

/**
 * Class ContentRolesRenderer.
 */
class ContentRolesRenderer {

  use StringTranslationTrait;

  /**
   * Drupal\user\PermissionHandlerInterface definition.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $userPermissions;

  /**
   * Drupal\content_roles\Service\ContentRolesManager definition.
   *
   * @var \Drupal\content_roles\Service\ContentRolesManager
   */
  protected $contentRolesManager;

  /** @var array */
  protected $allPermission;

  /**
   * Constructs a new ContentRolesRenderer object.
   */
  public function __construct(PermissionHandlerInterface $user_permissions, ContentRolesManager $content_roles_manager) {
    $this->userPermissions = $user_permissions;
    $this->contentRolesManager = $content_roles_manager;
    $this->allPermission = $this->userPermissions->getPermissions();
  }

  public function defaultPermissionList(ContentRole $role) {
    $items = [];
    foreach ($role->getPermissions() as $key => $permission) {
      $items[$key] = $this->allPermission[$key]['title'];
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => $this->t('Permissions'),
    ];
  }

}
