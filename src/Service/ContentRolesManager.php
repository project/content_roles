<?php

namespace Drupal\content_roles\Service;

use Composer\EventDispatcher\EventDispatcher;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\content_roles\Entity\ContentRole;
use Drupal\content_roles\Event\AddRemove;
use Drupal\content_roles\Event\AddRemoveEvents;
use Drupal\content_roles\Event\ContentRolesEventBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\user\Entity\User;
use Drupal\user\PermissionHandler;

/**
 * Class ContentRolesManager.
 */
class ContentRolesManager {

  /**
   * The entity type manager definition.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The User entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * The Content role entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $contentRoleStorage;

  /**
   * The Content role entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The permission handler service.
   *
   * @var \Drupal\user\PermissionHandler
   */
  protected $permissionHandler;

  /**
   * The event dispatcher service.
   *
   * @var \Composer\EventDispatcher\EventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Constructs a new ContentRolesManager object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              AccountProxy $current_user,
                              ConfigFactory $configFactory,
                              PermissionHandler $permissionHandler,
                              ContainerAwareEventDispatcher $eventDispatcher,
                              CacheBackendInterface $cacheBackend) {
    $this->entityTypeManager = $entity_type_manager;
    $this->userStorage = $this->entityTypeManager->getStorage('user');
    $this->contentRoleStorage = $this->entityTypeManager->getStorage('content_role');
    $this->currentUser = $current_user;
    $this->configFactory = $configFactory;
    $this->permissionHandler = $permissionHandler;
    $this->eventDispatcher = $eventDispatcher;
    $this->cache = $cacheBackend;
  }

  /**
   * Gets all attached content roles of a user.
   */
  public function getUsersContentRoles(User $user) {
    return $user->get('content_roles')->referencedEntities();
  }

  /**
   * Gets all permissions provided by content roles or optionally including the
   * config roles based permissions.
   *
   * @param \Drupal\user\Entity\User $user
   * @param bool $roles_included
   *
   * @return array|mixed
   */
  public function getUsersPermissions(User $user, bool $roles_included = FALSE) {
    $content_role_permissions = [];
    $config_role_permissions = [];

    $cache_tags = ['user:' . $user->id()];
    $cid = 'content_roles_content_role:' . $user->id();
    // The getUsersPermissions can be called many times during a request
    // (sometimes >1000 times). Static caching eliminates the need to retrieve
    // the cache from the assigned caching back-end.
    $static_cache = &drupal_static($cid, NULL);

    if (!empty($static_cache)) {
      $content_role_permissions = $static_cache;
    }
    elseif (!empty($this->cache->get($cid))) {
      $cache = $this->cache->get($cid);
      $static_cache = $cache->data;
      $content_role_permissions = $static_cache;
      $cache_tags = $cache->tags;
    }
    else {
      /** @var \Drupal\content_roles\Entity\ContentRole $role */
      foreach ($this->getUsersContentRoles($user) as $role) {
        $cache_tags[] = 'content_role:' . $role->id();
        $content_role_permissions = array_merge($content_role_permissions, $role->getPermissions());
      }
      $this->cache
        ->set($cid, $content_role_permissions, CacheBackendInterface::CACHE_PERMANENT, $cache_tags);
      $static_cache = $content_role_permissions;
    }

    if ($roles_included) {
      $cid = 'content_roles_config_roles:' . $user->id();
      $static_cache = &drupal_static($cid, NULL);
      if (!empty($static_cache)) {
        $config_role_permissions = $static_cache;
      }
      elseif (!empty($this->cache->get($cid))) {
        $cache = $this->cache->get($cid);
        $static_cache = $cache->data;
        $config_role_permissions = $static_cache;
      }
      else {
        $roles_permissions = $this->getUsersConfigRolePermissions($user);

        foreach ($roles_permissions as $permissions) {
          if (!empty($permissions)) {
            foreach ($permissions as $permission) {
              $config_role_permissions[] = $permission;
            }
          }
        }
        $this->cache
          ->set($cid, $config_role_permissions, CacheBackendInterface::CACHE_PERMANENT, $cache_tags);
        $static_cache = $config_role_permissions;
      }

      return array_merge($config_role_permissions, array_keys($content_role_permissions));
    }
    else {
      return $content_role_permissions;
    }
  }

  /**
   * Gets every permission of the current user.
   */
  public function getCurrentUsersPermissions() {
    $user = $this->userStorage->load($this->currentUser->id());
    return $this->getUsersPermissions($user, TRUE);
  }

  /**
   * Gets all permission of a user provided by her content roles.
   */
  public function getUsersConfigRolePermissions(User $user) {
    $user_roles = $user->getRoles();
    $permissions = [];
    foreach ($user_roles as $rid) {
      $role = $this->loadConfigRole($rid);
      $permissions[$rid] = $role->get('permissions');
    }

    return $permissions;
  }

  /**
   * Gets all permission of a user provided by her content roles grouped by the
   * role id.
   */
  public function getUsersGroupedPermissions(User $user) {
    $permissions['config_roles'] = $this->getUsersConfigRolePermissions($user);
    /** @var \Drupal\content_roles\Entity\ContentRole $role */
    foreach ($this->getUsersContentRoles($user) as $role) {
      $permissions['content_roles'][$role->id()] = $role->getPermissions();
    }

    return $permissions;
  }

  /**
   * Loads a role configuration.
   */
  public function loadConfigRole($rid) {
    $config_key = "user.role.$rid";
    return  $this->configFactory->get($config_key);
  }

  /**
   * Gets all permissions of a user from the permissionhandler
   */
  public function getAllPermissions() {
    return $this->permissionHandler->getPermissions();
  }

  /**
   * Adds a content_role to a user.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addUserToContentRole(User $user, ContentRole $contentRole, $save = FALSE) {
    /** @var ContentRole $usersAttachedRole */
    // We don't want to add a content role twice.
    foreach ($this->getUsersContentRoles($user) as $usersAttachedRole) {
      if ($usersAttachedRole->id() == $contentRole->id()) {
        return;
      }
    }

    $user->content_roles->appendItem($contentRole->id());
    if ($save) {
      $user->setValidationRequired(TRUE);
      $user->validate();
      $user->save();
    }

    // Invalidate the static cache.
    $cid = 'content_roles_content_role:' . $user->id();
    drupal_static_reset($cid);
  }

  /**
   * Adds a content_role to a user.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeUserFromContentRole(User $user, ContentRole $contentRole, $save = FALSE) {
    $content_roles = $user->content_roles->referencedEntities();

    foreach ($content_roles as $key => $content_role) {
      if ($contentRole == $content_role) {
        $user->content_roles->removeItem($key);
        if ($save) {
          $user->setValidationRequired(TRUE);
          $user->save();
          // Invalidate the static cache.
          $cid = 'content_roles_content_role:' . $user->id();
          drupal_static_reset($cid);
        }
        break;
      }
    }
  }

}
