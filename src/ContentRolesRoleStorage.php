<?php

namespace Drupal\content_roles;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\content_roles\Service\ContentRolesManager;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\user\RoleStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller class for user content roles.
 */
class ContentRolesRoleStorage extends RoleStorage {

  /**
   * The Config factory service.
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The UUID service.
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The Language manager service.
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The current user.
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The ContentRoles manager service.
   * @var \Drupal\content_roles\ContentRolesManager
   */
  protected $contentRolesManager;

  /**
   * Constructs a ConfigEntityStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface                  $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface               $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface                     $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Language\LanguageManagerInterface           $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface|null $memory_cache
   *   The memory cache backend.
   */
  public function __construct(EntityTypeInterface $entity_type,
                              ConfigFactoryInterface $config_factory,
                              UuidInterface $uuid_service,
                              LanguageManagerInterface $language_manager,
                              MemoryCacheInterface $memory_cache = NULL,
                              AccountProxy $current_user = NULL,
                              ContentRolesManager $contentRolesManager = NULL) {
    parent::__construct($entity_type, $config_factory, $uuid_service, $language_manager, $memory_cache);

    $this->configFactory = $config_factory;
    $this->uuidService = $uuid_service;
    $this->languageManager = $language_manager;
    $this->currentUser = $current_user;
    $this->contentRolesManager = $contentRolesManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('config.factory'),
      $container->get('uuid'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('current_user'),
      $container->get('content_roles.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function isPermissionInRoles($permission, array $rids) {
    $has_permission = FALSE;

    // This is the core's original behavior.
    foreach ($this->loadMultiple($rids) as $role) {
      /** @var \Drupal\user\RoleInterface $role */
      if ($role->isAdmin() || $role->hasPermission($permission)) {
        $has_permission = TRUE;
        break;
      }
    }

    $content_roles_permissions = $this->contentRolesManager->getCurrentUsersPermissions();
    if (in_array($permission, $content_roles_permissions)) {
      $has_permission = TRUE;
    }

    return $has_permission;
  }

}
