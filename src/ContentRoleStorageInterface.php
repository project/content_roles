<?php

namespace Drupal\content_roles;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\content_roles\Entity\ContentRoleInterface;

/**
 * Defines the storage handler class for Content role entities.
 *
 * This extends the base storage class, adding required special handling for
 * Content role entities.
 *
 * @ingroup content_roles
 */
interface ContentRoleStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Content role revision IDs for a specific Content role.
   *
   * @param \Drupal\content_roles\Entity\ContentRoleInterface $entity
   *   The Content role entity.
   *
   * @return int[]
   *   Content role revision IDs (in ascending order).
   */
  public function revisionIds(ContentRoleInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Content role author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Content role revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\content_roles\Entity\ContentRoleInterface $entity
   *   The Content role entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ContentRoleInterface $entity);

  /**
   * Unsets the language for all Content role with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
