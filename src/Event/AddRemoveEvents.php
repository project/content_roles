<?php

namespace Drupal\content_roles\Event;

use Composer\EventDispatcher\Event;
use Drupal\content_roles\Entity\ContentRole;
use Drupal\user\Entity\User;

/**
 * Contains Drupal\content_roles\Event.
 */
class AddRemoveEvents extends Event {

  const ADD_ROLE_TO_USER = 'content_role.role.add';

  const REMOVE_ROLE_FROM_USER = 'content_role.role.remove';

  /**
   * The content role entity.
   *
   * @var \Drupal\content_roles\Entity\ContentRole
   */
  protected $contentRole;

  /**
   * The user entity.
   * @var \Drupal\user\Entity\User
   */
  protected $account;

  /**
   * Constructs a node insertion demo event object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function __construct(ContentRole $contentRole, User $user) {
    $this->contentRole = $contentRole;
    $this->account = $user;
  }

  /**
   * @return \Drupal\content_roles\Entity\ContentRole
   */
  public function getContentRole(): \Drupal\content_roles\Entity\ContentRole {
    return $this->contentRole;
  }

  /**
   * @return \Drupal\user\Entity\User
   */
  public function getAccount(): \Drupal\user\Entity\User {
    return $this->account;
  }

}
