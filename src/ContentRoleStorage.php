<?php

namespace Drupal\content_roles;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\content_roles\Entity\ContentRoleInterface;
use Drupal\user\Entity\User;

/**
 * Defines the storage handler class for Content role entities.
 *
 * This extends the base storage class, adding required special handling for
 * Content role entities.
 *
 * @ingroup content_roles
 */
class ContentRoleStorage extends SqlContentEntityStorage implements ContentRoleStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ContentRoleInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {content_role_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {content_role_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ContentRoleInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {content_role_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('content_role_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
