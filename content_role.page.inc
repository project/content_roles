<?php

/**
 * @file
 * Contains content_role.page.inc.
 *
 * Page callback for Content role entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Content role templates.
 *
 * Default template: content_role.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_content_role(array &$variables) {
  // Fetch ContentRole Entity Object.
  $content_role = $variables['elements']['#content_role'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
